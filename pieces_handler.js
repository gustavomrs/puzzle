/**
 * Javascript for drag-and-drop puzzle question
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright 2015 (Gustavo Rocha, Maicon Gobbi, Emanuel Melechco)
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

YUI().add(
    'puzzle_dd',
    function(Y) {
        // Evento drop:over (soltar sobre ou semelhante).
        Y.DD.DDM.on('drop:over', function(e) {
            /*  Drag é a referencia da peça que está sendo arrastada
                Drop é a referencia da peça que será trocada com a 
                arrastada caso a arrastada seja solta */
            var drag = e.drag.get('node'),
                drop = e.drop.get('node');
        });
        
        /* Evento drag:drag, esse evento pega todos os eventos de "arraste"
        de uma peça, ou seja, toda vez que uma peça é arrastada, sua posição 
        muda e esse evento é lançado */
        Y.DD.DDM.on('drag:drag', function(e) {
            //Pegando a ultima posição no eixo y
            var y = e.target.lastXY[1];
            //Verificando a diferença entre as posições do y
            if (y < lastY) {
                //se o y atual for maior, o usuário está subindo a peça
                goingUp = true;
            } else {
                //caso contrário o usuário está descendo a peça 
                goingUp = false;
            }
            //Guardando a posição atual do y como a ultima
            lastY = y;
        });
        
        /* Esse evento é lançado sempre que uma imagem começa a ser arrastada*/
        Y.DD.DDM.on('drag:start', function(e) {
            //Armazenando qual peça está sendo arrastada
            var drag = e.target;
            
            // Diferenciando a imagem que está sendo arrastada
            // Opacidade setada pra 0 (a imagem some enquanto é arrastada)
            drag.get('node').setStyle('opacity', '1');
            
        });
        
        /* Esse é evento é lançado quando um arraste é concluido*/
        Y.DD.DDM.on('drag:end', function(e) {
            var drag = e.target;
            // Voltando os estilos originais da imagem quando o arrastar é finalizado
            drag.get('node').setStyles({
                visibility: '',
                opacity: '1'
            });
        });
                    
        //Variáveis Estáticas
        var goingUp = false, lastY = 0;
    
    },
    '1.0.0',
    {requires: ['dd-constrain', 'dd-proxy', 'dd-drop']}
);
    
    
M.pieces_handler = {};

M.pieces_handler.init = function(Y, options) {

    YUI().use('puzzle_dd', function(Y) {

        // Pegando o id da questao
        var queid = options.queid;

        // Criando um seletor base
        var baseSelector = '#' + queid;
        
        /*  Pegando a div do puzzle com a classe puzzle, 
            que está dentro da div com o id "queid" */
        var puzzle = Y.Node.all(baseSelector + ' .puzzle');

        /*  Pegando todos os slots (peças) e tornado-os 
            possiveis de serem preenchidos por uma imagem */
        var slots = Y.Node.all(baseSelector + ' .puzzle .ddslot');
        slots.each(function(slot, k) {
            new Y.DD.Drop({ node: slot });
        });    

        /*  Criando as peças do quebra-cabeça
        */
        var pieces = Y.Node.all(baseSelector + ' .puzzle .ddpiece');
        pieces.each(function(piece, k) {
            new Y.DD.Drag({
                node: piece
            }).plug(Y.Plugin.DDProxy, {
                moveOnEnd: false
            }).plug(Y.Plugin.DDConstrained, {
                constrain2node: puzzle
            });
        });

        /*  Evento que é causado sempre que uma peça é solta dentro de um slot
            (obs: Apenas dentro de um slot! De outra maneira esse evento não é disparado) */ 
        Y.DD.DDM.on('drag:drophit', function(e) {
            var drop = e.drop.get('node'),
                drag = e.drag.get('node');

            /* Verificando se o slot no qual a peça foi colocada é um slot
            valido (div com a classe ddslot) */
            if (drop.get('className').search('ddslot') != -1) {
                /* Trocando duas peças de lugar, colocando cada uma em seus
                respectivos slots */
                drag.get('parentNode').appendChild(drop.one('.ddpiece'));
                drop.append(drag);

                /*  Recalculando ordem das peças nos slots 
                    dentro do array order */
                var slots = drop.get('parentNode').all('.slot'),
                    order = [];
                slots.each(function(slot, k) {
                    order.push(slot.one('.piece').getAttribute('value'));
                });
                
                /*  Armazenando nova a ordem das peças após uma mudança de qualquer posição.
                    O numero armazenado na ordem das peças é baseado no numero inicial de cada peça.

                    Por exemplo: Se o quebra cabeça possui 9 peaças, a ordem inicial é:
                    0, 1, 2, 3, 4, 5, 6, 7, 8. Se a primeira peça for trocada com a segunda,
                    a nova ordem será: 1, 0, 2, 3, 4, 5, 6, 7, 8 */
                drop.get('parentNode').get('parentNode').one('input').set('value', order.join(','));                
            }

        });
    });
};
