<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * puzzle question renderer class.
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Gera a saida da questao puzzle.
 *
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_puzzle_default_renderer extends qtype_renderer {
    
    /**
     * Prepara o formulario do quebra cabeca  
     */
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {

        //recupera os dados da questao
        $question = $qa->get_question();
        
        //recupera os dados da resposta, se tiver respondido alguma vez
        $response = $qa->get_last_qt_var('answer');
        
        //recuperando a imagem
        $image = $qa->rewrite_pluginfile_urls($question->image, 'qtype_puzzle', 'image', $question->id);
        
        //recuperando o tamanho da imagem
        list($width, $height) = $question->dimensions;
        
        //recupera as colunas e linhas
        list($cols, $rows) = $question->colsrows;
        
        //largura do painel da peca
        $panelwidth = floor($width / $cols);
        
        //altura do painel da peca
        $panelheight = floor($height / $rows);
        
        //dimensao do painel da peca
        $paneldimension = "width: {$panelwidth}px; height: {$panelheight}px;";
        
        //configuracao das pecas
        $orderpieceimage = $question->get_orderpieceimage($qa);
        $painelpiece = array();
        $pieces = array();
        
        foreach ($orderpieceimage as $key => $slot) {
            $left = (($slot % $cols) * -(floor($width / $cols)));
            $top = (floor($slot / $cols) * -(floor($height / $rows)));
            $sprite = "background: url($image) {$left}px {$top}px no-repeat;";
            
            $painelpiece[] = array(
                'class' => "slot ddslot",
                'style' => $paneldimension
            );
             
            $pieces[] = array(
                'class' => "piece ddpiece",
                'value' => $key,
                'style' => $paneldimension. $sprite
            );             
        }
        
        // Verifica se a question ja foi respondida
        if (!empty($response)) {
            $resporderpiece = explode(',', $response);
        } else {
            $resporderpiece = $this->begin_response_orderpieces($question, count($orderpieceimage));
        }
        
        // cria o html do quebra cabeca
        $puzzle = $this->create_puzzle_html($question, $painelpiece, $pieces, array_fill(0, count($orderpieceimage), ''), $resporderpiece);
        
        //Resposta da question
        $inputname = $qa->get_qt_field_name('answer');
        $inputattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname,
        );
        $inputelement = html_writer::empty_tag('input', $inputattributes);
  
        $this->additional_formulation_and_controls($qa, $options);        
        return $this->create_html_question($qa, $puzzle. $inputelement);
    }
    
    /**
     * Retorna um range de 0 ate o total das pecas -1   
     */
    protected function begin_response_orderpieces($question, $count) {
        return range(0, $count - 1);
    }
    
    /**
     * Adiciona o arquivo js que controlara o drag and drop das pecas  
     */
    protected function additional_formulation_and_controls($qa, question_display_options $options) {
        global $PAGE;
        
        $optionsdragdrop = array(
            'queid' => 'q'. $qa->get_slot()
        );
        
        //utilizando o javascript "pieces handler"
        if (!$options->readonly) {
            $module = array(
                'name' => 'M.pieces_handler',
                'fullpath' => '/question/type/puzzle/pieces_handler.js',
                'requires' => array('yui3-yahoo-dom-event', 'yui3-dragdrop', 'yui3-animation'));
                
            $PAGE->requires->js_init_call('M.pieces_handler.init', array($optionsdragdrop), true, $module);
        }
    }
    
    /**
     * Cria o html de toda a questao  
     */
    protected function create_html_question(question_attempt $qa, $content) {
        
        $question = $qa->get_question();
        
        $result = '';
        $result .= html_writer::tag('div', $question->format_questiontext($qa), array('class' => 'qtext'));
        
        // block
        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
        $result .= html_writer::tag('div', get_string('prompt', 'qtype_puzzle'), array('class' => 'prompt'));

        // answer
        $result .= html_writer::tag('div', $content, array('class' => 'answer'));

        // close block
        $result .= html_writer::end_tag('div');

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($qa->get_last_qt_data()),
                    array('class' => 'validationerror'));
        }

        return $result;        
        
    }
    
    /**
     * Cria o html do quebra cabeca
     */
    protected function create_puzzle_html($question, $slots, $pieces, $feedbackimgs, $resporder) {        
        
        // painel
        $paineldivs = array();
        foreach ($resporder as $key => $slotkey) {
            $piece = html_writer::tag('div', $feedbackimgs[$key], $pieces[$slotkey]);        
            $paineldivs[] = html_writer::tag('div', $piece, $slots[$key]);
        }
        
        // puzzle
        list($width, $height) = $question->dimensions;
        $puzzle = html_writer::tag(
            'div',
            implode("\n", $paineldivs),
            array('class' => 'puzzle',
                  'style' => "width: {$width}px; height: {$height}px;")
        );
        return $puzzle;
    }

    /**
     * Verifica se a resposta da questao esta correta
     */
    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();
        $image = $qa->rewrite_pluginfile_urls($question->image, 'qtype_puzzle', 'image', $question->id);
        list($width, $height) = $question->dimensions;
        
        $attr = array();
        $attr[] = "width:{$width}px;";
        $attr[] = "height:{$height}px;";
        $attr[] = "background: url($image) no-repeat;";
        return html_writer::tag('div', null, array('style' => implode('', $attr)));
    }
}
