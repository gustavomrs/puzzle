<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * puzzle question definition class.
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Represents a puzzle question.
 *
 * @copyright  TEAM_UFMS

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_puzzle_question extends question_graded_automatically {
    
    //armazena o tamanho da imagem
    public $dimensions;
    
    //armazena as colunas e linhas 
    public $colsrows;
    
    //armazena a imagem
    public $image;
    
    //armazena a ordem da imagem
    protected $orderpieceimage = null;
    
    /**
     * Inicia uma nova tentativa para esta questão, armazenando todas as 
     * informações que serão necessárias mais tarde
     */
    public function start_attempt(question_attempt_step $step, $variant) {
        
        //recebe as linhas e colunas que foram definidas na questao
        list($cols, $rows) = $this->colsrows;
        
        //armazena a ordem das pecas imagem
        $this->orderpieceimage = range(0, ($cols * $rows) - 1);
        
        //embaralha a ordem das pecas
        shuffle($this->orderpieceimage);
        
        //recuperando a imagem salva na questao
        $fs = get_file_storage();
        $images = $fs->get_area_files($this->contextid, 'qtype_puzzle', 'image', $this->id, 'sortorder', false);   
        $imageref = key($images);
        
        $imagename = $images[$imageref]->get_filename();
        $this->image = "@@PLUGINFILE@@/$imagename";
        
        //define uma variavel do tipo da pergunta em cache
        $step->set_qt_var('_orderpieceimage', $imagename. ','. implode(',', $this->orderpieceimage));        
        
    }
    
    /**
     * Inicia a questao, voltando ao passo que foi salvo na ultima tentativa
     */
    public function apply_attempt_state(question_attempt_step $step) {
        
       $this->orderpieceimage = explode(',', $step->get_qt_var('_orderpieceimage'));
       $imagename = array_shift($this->orderpieceimage);
       $this->image = "@@PLUGINFILE@@/$imagename";
    }
    
    /**
     * Inicializa a sequencia das pecas da imagem
     */
    public function get_orderpieceimage(question_attempt $qa) {
        
        //se a sequencia esta nula, inicia a ordem de da ultima tentativa
        if (is_null($this->orderpieceimage)) {
            $this->apply_attempt_state($qa->get_step(0));
        }
        
        return $this->orderpieceimage;
    }

    /*
     * Retorna quais dados podem ser incluidos na submissao do formulario 
     * quando um estudante submete
     */
    public function get_expected_data() {
        return array('answer' => PARAM_SEQUENCE);
    }
    
    /**
     * Renderiza a pagina que sera mostrada na questao
     */
    public function get_renderer(moodle_page $page) {
        return $page->get_renderer('qtype_puzzle', 'default');
    }

    /*
     * Produz um resumo da resposta
     */
    public function summarise_response(array $response) {
       //verifica se a variavel foi definida e retorna ela
        if (isset($response['answer'])) {
            return $response['answer'];
        } 
        
        //se a variavel nao foi definida retornara nulo
        return null;
    }

    /*
     * Verifica se o puzzle foi completado com sucesso
     */
    public function is_complete_response(array $response) {
        
        //retorna verdadeiro se o puzzle foi montado corretamente
        //e falso se o puzzle nao foi montado corretamente
        $respfinish = isset($response['answer']) and ($response['answer'] or $response['answer'] === '0');
        
        return $respfinish; 
    }

    /*
     * Este metodo verifica se o estudante forneceu as respostas suficientes
     * para a questao
     */
    public function get_validation_error(array $response) {
        
        if ($this->is_gradable_response($response)) {
            return '';
        }
        
        //retorna o que nao foi respondido na questao para finaliza-la
        return get_string('responserequest', 'qtype_puzzle');
    }

    /*
     * Determina se a resposta do estudante foi alterada ou nao
     */
    public function is_same_response(array $prevresponse, array $newresponse) {
       return question_utils::arrays_same_at_key_missing_is_blank(
            $prevresponse, $newresponse, 'answer');
    }

    /**
     * Retorna a resposta correta da questao     
     */
    public function get_correct_response() {
       
        // recebe a sequencia da imagem
        $orderpieceimage = $this->orderpieceimage;
        
        // ordena a sequencia da imagem
        asort($orderpieceimage);
                
        return array('answer' => implode(',', array_keys($orderpieceimage)));
    }

    /**
     * Verifica se o usuario pode acessar a um arquivo particular
     */
    public function check_file_access($qa, $options, $component, $filearea,
            $args, $forcedownload) {
        
        if ($component == 'qtype_puzzle' && $filearea == 'image') {
            $questionid = reset($args);
            return $questionid == $this->id;

        } else {
            return parent::check_file_access($qa, $options, $component, $filearea,
                    $args, $forcedownload);
        }
    }

    /*
     * Retorna se a imagem foi montada corretamente = 1
     * se nao foi montada corretamente = 0
     */
    public function grade_response(array $response) {
              
        $correctimage = 0;
        
        //recebe a quantidade total de pedacos em que a imagem foi recortada e
        //a quantidade de pedacos que foi montaoa corretamente
        list($respright, $resptotal) = $this->correct_parts_image($response);
        
        //Se a quantidade dos pedacos da imagem recortada bater com a quantidad total
        //a resposta esta correta
        if ($respright == $resptotal) {
            $correctimage = 1;
        }
        
        return array($correctimage, question_state::graded_state_for_fraction($correctimage));
    }
    
    /**
     * Retorna o total dos pedacos em que a imagem foi recortada
     * e a quantidade dos pedacos da imagem que foram montados corretamente
     */
    public function correct_parts_image($response) {
        
        $piecescorrect = 0;
        
        //se a resposta nao estiver vazia
        if (!empty($response['answer'])) {
            
            //recebe a ordem da sequencia das pecas da imagem que o aluno respondeu
            $orderpieceimage = explode(',', $response['answer']);
            
            foreach ($orderpieceimage as $piece => $orderresp) {
                
                //se a ordem das pecas nao esta vazia,
                if ($orderresp !== '') {
                    
                    //se a peca esta na order da imagem correta, o numero
                    //de pecas corretas e incrementado
                    if ($piece == $this->orderpieceimage[$orderresp]) {
                        $piecescorrect ++;
                    }
                }
            }                       
        }
        
        //total de pecas da imagem
        $totalpieces = count($this->orderpieceimage);
        
        return array($piecescorrect, $totalpieces);
        
    }
   
}
