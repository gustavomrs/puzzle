<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Question type class for the puzzle question type.
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/questionlib.php');
require_once($CFG->dirroot . '/question/engine/lib.php');
require_once($CFG->dirroot . '/question/type/puzzle/question.php');


/**
 * The puzzle question type.
 *
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_puzzle extends question_type {
    
    public function extra_question_fields() {
        return array('question_puzzle', 'dimensions', 'colsrows');
    }

    public function questionid_column_name() {
        return 'question';
    }
    

    public function move_files($questionid, $oldcontextid, $newcontextid) {
        parent::move_files($questionid, $oldcontextid, $newcontextid);
        //$this->move_files_in_hints($questionid, $oldcontextid, $newcontextid);
        $fs = get_file_storage();
        $fs->move_area_files_to_new_context($oldcontextid, $newcontextid, 'qtype_puzzle', 'image', $questionid);
    }

    protected function delete_files($questionid, $contextid) {
        parent::delete_files($questionid, $contextid);
        //$this->delete_files_in_hints($questionid, $contextid);
        $fs = get_file_storage();
        $fs->move_area_files_to_new_context($oldcontextid, $newcontextid, 'qtype_puzzle', 'image', $questionid);        
    }

    public function save_question_options($question) {
        global $DB;
        // question context
        $context = $question->context;
        
        //Recupera as informacoes que o usuario informou 
        $question->dimensions = implode(',', $question->dimensions);
        $question->colsrows = implode(',', $question->colsrows);
        
        //salvando dados da questao
        $save = parent::save_question_options($question);
        if ($save !== null) {
            return $save;
        }

        //salvando a imagem
        file_save_draft_area_files(
            $question->upload, 
            $context->id,
            'qtype_puzzle',
            'image',
            $question->id,
            array('subdirs' => 0, 'maxbytes' => 0, 'maxfiles' => -1)
        );
        return null;
        //$this->save_hints($question);
    }

    protected function initialise_question_instance(question_definition $question, $questiondata) {
       parent::initialise_question_instance($question, $questiondata);
        $question->dimensions = explode(',', $questiondata->options->dimensions);
        $question->colsrows = explode(',', $questiondata->options->colsrows);
    }

    public function get_random_guess_score($questiondata) {
        // TODO.
        return 0;
    }

    public function get_possible_responses($questiondata) {
        // TODO.
        return array();
    }
}
