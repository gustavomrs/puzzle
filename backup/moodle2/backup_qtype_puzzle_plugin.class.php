<?php

defined('MOODLE_INTERNAL') or die;

class backup_qtype_puzzle_plugin extends backup_qtype_plugin {

    public static function get_qtype_fileareas() {
        return array('image' => 'question_created');
    }

    /**
     * Returns the qtype information to attach to question element
     */
    protected function define_question_plugin_structure() {

        // Define the virtual plugin element with the condition to fulfill
        $plugin = $this->get_plugin_element(null, '../../qtype', 'puzzle');

        // Create one standard named plugin element (the visible container)
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // connect the visible container ASAP
        $plugin->add_child($pluginwrapper);


        ///estrutura da questão
        //verificar, quais deles serão usados e como serão usados
        /*
        // Now create the qtype own structures
        $puzzle = new backup_nested_element();
     
        $pluginwrapper->add_child($puzzle);

        // set source to populate the data
        $puzzle->set_source_table('question_puzzle', array('question' => backup::VAR_PARENTID));

        // file annotation
        $puzzle->annotate_files('qtype_puzzle', 'image', 'id');

*/

        return $plugin;
    }

}
