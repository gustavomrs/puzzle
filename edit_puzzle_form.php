<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the editing form for the puzzle question type.
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright  TEAM_UFMS(EMANUEL, GUSTAVO, MAICON)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * puzzle question editing form definition.
 *
 * @copyright  TEAM_UFMS(EMANUEL, GUSTAVO, MAICON)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_puzzle_edit_form extends question_edit_form {

    /**
     * Prepara o formulario da questao puzzle
     * @param type $mform
     */
    protected function definition_inner($mform) {
        // Puzzle fields
        $mform->addElement('header', 'puzzlesettings', get_string('puzzlesettings', 'qtype_puzzle'));
        // input for upload a file
        $mform->addElement('filepicker', 'upload', get_string('upload', 'qtype_puzzle'), null,
                    array('maxbytes' => 5000000, 'accepted_types' => 'image'));        
                
        //adiciona o componente image dimensions no formulario
        $grp = array();
        $grp[] = &$mform->createElement('text', 'dimensions[width]', null, array('size'=>'8'));
        $grp[] = &$mform->createElement('text', 'dimensions[height]', null, array('size'=>'8'));
        $mform->addGroup($grp, 'dimensionsgrp', 
                            get_string('dimensions', 'qtype_puzzle'), 
                            array(' x '), false);
        $mform->setType('dimensions[width]', PARAM_INT);
        $mform->setType('dimensions[height]', PARAM_INT);
        $mform->setDefault('dimensions[width]', 600);
        $mform->setDefault('dimensions[height]', 400);
        $mform->addGroupRule('dimensionsgrp', array('dimensions[width]' => array(array(null, 'numeric', null, 'client'))));
        $mform->addGroupRule('dimensionsgrp', array('dimensions[height]' => array(array(null, 'numeric', null, 'client'))));
        
        //adiciona o componente cols rows no formulario
        $grp=array();
        $grp[] = &$mform->createElement('text', 'colsrows[cols]', null, array('size'=>'8'));
        $grp[] = &$mform->createElement('text', 'colsrows[rows]', null, array('size'=>'8'));
        $mform->addGroup($grp, 'colsrowsgrp', 
                            get_string('colsrows', 'qtype_puzzle'), 
                            array(' x '), false);
        $mform->setType('colsrows[cols]', PARAM_INT);
        $mform->setType('colsrows[rows]', PARAM_INT);
        $mform->setDefault('colsrows[cols]', 3);
        $mform->setDefault('colsrows[rows]', 3);
        $mform->addGroupRule('colsrowsgrp', array('colsrows[cols]' => array(array(null, 'numeric', null, 'client'))));
        $mform->addGroupRule('colsrowsgrp', array('colsrows[rows]' => array(array(null, 'numeric', null, 'client'))));
        
        $this->add_interactive_settings();
               
    }

    /**
     * Processa os dados da questao no formulario
     * @param type $question
     * @return type
     */
    protected function data_preprocessing($question) {
        $question = parent::data_preprocessing($question);
        
        if (!empty($question->options)) {
            
            //recupera as dimensoes
            list($question->{'dimensions[width]'},
                 $question->{'dimensions[height]'}) = explode(',', $question->options->dimensions);
            
            //recupera as colunas e linhas 
            list($question->{'colsrows[cols]'},
                 $question->{'colsrows[rows]'}) = explode(',', $question->options->colsrows);
        }

        // image files
        $fmoptions = array('subdirs' => 0, 'maxbytes' => 0, 'maxfiles' => -1, 'accepted_types' => array('image'));//da para definir os tipos aceitos aqui
        $draftitemid = file_get_submitted_draft_itemid('upload');
        $questionid = !empty($question->id) ? $question->id : 0;
        file_prepare_draft_area($draftitemid, $this->context->id, 'qtype_puzzle', 'image', $questionid, $fmoptions);
        $question->upload = $draftitemid;      

        return $question;
    }
    
    /**
     * Neste metodo sao realizados as validacoes da questao
     * @global type $USER
     * @param type $data
     * @param type $files
     * @return type
     */
    public function validation($data, $files) {
        
        global $USER;
        $errors = parent::validation($data, $files);
        
        //Validando as dimensoes da imagem
        if (!($data['dimensions']['width'] > 1)) {
            $errors['dimensionsgrp'] = get_string('err_greaterthan', 'qtype_puzzle', 1);
        }
        if (!($data['dimensions']['height'] > 1)) {
            $errors['dimensionsgrp'] = get_string('err_greaterthan', 'qtype_puzzle', 1);
        }
        
        //Validando as linhas e colunas
         if ($data['colsrows']['cols'] == 1 and $data['colsrows']['rows'] == 1) {
            $errors['colsrowsgrp'] = get_string('err_notbeone', 'qtype_puzzle');
        }
        if (!($data['colsrows']['cols'] < $data['dimensions']['width'])) {
            $errors['colsrowsgrp'] = get_string('err_lowerthan', 'qtype_puzzle', $data['dimensions']['width']);
        }
        if (!($data['colsrows']['rows'] < $data['dimensions']['height'])) {
            $errors['colsrowsgrp'] = get_string('err_lowerthan', 'qtype_puzzle', $data['dimensions']['height']);
        }
        
        //Validando se a imagem foi selecionada
        $fs = get_file_storage();
        $usercontext = get_context_instance(CONTEXT_USER, $USER->id);
        if (!$files = $fs->get_area_files($usercontext->id, 'user', 'draft', $data['upload'], 'sortorder', false)) {
            $errors['upload'] = get_string('error_image', 'qtype_puzzle');
        }
        
        return $errors;

    }
    
    /**
     * Retorna o tipo da questao
     * @return string
     */
    public function qtype() {
        return 'puzzle';
    }
}
