<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_puzzle', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    qtype
 * @subpackage puzzle
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'puzzle';
$string['pluginname_help'] = 'Create a cloze question type with embedded response fields in the question text to enter a numeric
or text value or select a value from a number of options.';
$string['pluginname_link'] = 'question/type/puzzle';
$string['pluginnameadding'] = 'Adding a puzzle question';
$string['pluginnameediting'] = 'Editing a puzzle question';
$string['pluginnamesummary'] = 'A puzzle question type allows a puzzle question by an uploaded image';
$string['puzzlesettings'] = 'Puzzle Settings';
$string['prompt'] = 'Complete the following image puzzle:';
$string['err_lowerthan'] = 'This values must be lower than {$a}';
$string['err_greaterthan'] = 'This values must be greater than {$a}';
$string['err_notbeone'] = 'Cols or rows should be greater than 1';


$string['upload'] = 'Image upload';
$string['dimensions'] = 'Image dimensions';
$string['colsrows'] = 'Column rows';
$string['error_image']='Please, report an image to the puzzle';
